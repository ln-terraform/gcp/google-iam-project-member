module "project-iam-permission" {
  source = "../.."

  google_accounts_permissions = [
    {
      google_account = "linhheo.cloud5@gmail.com",
      roles          = ["roles/owner"]
    },
    {
      google_account = "linh1612340@gmail.com",
      roles          = ["roles/owner"]
    }
  ]

  service_accounts_permissions = [
    {
      service_account = "gcloud-study-admin-service-acc@gcloud-study-308309.iam.gserviceaccount.com",
      roles           = ["roles/owner"]
    }
  ]
}