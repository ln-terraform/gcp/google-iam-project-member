variable "project" {
  type=string
}

variable "google_accounts_permissions" {
  type = list(object({
    google_account = string
    roles          = list(string)
  }))
  default = []
}

variable "service_accounts_permissions" {
  type = list(object({
    service_account = string
    roles           = list(string)
  }))
  default = []
}

variable "google_groups_permissions" {
  type = list(object({
    google_group = string
    roles        = list(string)
  }))
  default = []
}

variable "gsuite_domains_permissions" {
  type = list(object({
    gsuite_domain = string
    roles         = list(string)
  }))
  default = []
}